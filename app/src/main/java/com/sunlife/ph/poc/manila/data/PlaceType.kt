package com.sunlife.ph.poc.manila.data

/**
 * Created by keith on 13/03/2018.
 */
class PlaceType{
    var placeTypeName : String = ""
    var placeTypeId : Int = -1
    var placeTypeImg : Int = -1

    constructor(placeTypeName :String,placeTypeImg : Int){
        this.placeTypeName = placeTypeName
        this.placeTypeImg = placeTypeImg
    }
}