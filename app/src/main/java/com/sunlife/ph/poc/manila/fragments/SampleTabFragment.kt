package com.sunlife.ph.poc.manila.fragments

import com.sunlife.ph.poc.manila.R

/**
 * Created by keith on 07/03/2018.
 */
class SampleTabFragment : MainTabFragment() {
    override fun getTabIcon(): Int {
        return R.drawable.ic_pin
    }

    override fun getTabIconActive(): Int {
        return R.drawable.ic_pin_active
    }

    override fun getTabName(): String {
        return "Sample"
    }
}