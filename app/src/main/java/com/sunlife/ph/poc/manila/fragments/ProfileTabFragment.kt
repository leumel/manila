package com.sunlife.ph.poc.manila.fragments

import com.sunlife.ph.poc.manila.R

/**
 * Created by keith on 12/03/2018.
 */
class ProfileTabFragment : MainTabFragment(){
    override fun getTabName(): String {
        return "Profile"
    }

    override fun getTabIcon(): Int {
        return R.drawable.ic_user
    }

    override fun getTabIconActive(): Int {
        return  R.drawable.ic_user_active
    }

}