package com.sunlife.ph.poc.manila.adapters.fragmentpager

import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.View
import com.sunlife.ph.poc.manila.fragments.MainTabFragment
import com.sunlife.ph.poc.manila.views.CustomTab

/**
 * Created by keith on 07/03/2018.
 */
class CustomFragmentPagerAdapter : FragmentPagerAdapter, TabLayout.OnTabSelectedListener {

    var fragments : List<MainTabFragment>
    var context : Context
    var onClickListener : View.OnClickListener

    constructor(fragments: List<MainTabFragment>, context: Context, fm: FragmentManager, onClickListener: View.OnClickListener) : super(fm) {
        this.fragments = fragments
        this.context = context
        this.onClickListener = onClickListener
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun setupTabs(tabs: TabLayout) {
        var x = 0
        while(x < tabs.tabCount){
            tabs.getTabAt(x)?.customView = getCustomViewForFragment(x++)
        }
        tabs.setOnTabSelectedListener(this)
    }

    private fun getCustomViewForFragment(index:Int): View {
        var fragment = fragments[index]
        var customTab = CustomTab(context, fragment.getTabName(), fragment.getTabIcon(), fragment.getTabIconActive(), onClickListener)
        if (index == 0){
            customTab.toggle(true)
        }
        return  customTab
    }


    override fun onTabReselected(tab: TabLayout.Tab?) {
        Log.d("lem", "reselected")
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        var customTab  = tab?.customView as CustomTab
        customTab.toggle(false)
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        var customTab  = tab?.customView as CustomTab
        customTab.toggle(true)
    }

}