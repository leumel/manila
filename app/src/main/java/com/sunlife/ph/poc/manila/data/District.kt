package com.sunlife.ph.poc.manila.data

/**
 * Created by keith on 13/03/2018.
 */
class District{
    var districtName = ""
    var districtId = -1

    constructor(districtName : String, districtId : Int){
        this.districtName = districtName
        this.districtId = districtId
    }
}