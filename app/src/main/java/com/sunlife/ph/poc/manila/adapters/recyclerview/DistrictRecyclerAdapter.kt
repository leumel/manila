package com.sunlife.ph.poc.manila.adapters.recyclerview

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager
import com.sunlife.ph.poc.manila.OnItemClicked
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.data.District
import com.sunlife.ph.poc.manila.data.Place
import com.sunlife.ph.poc.manila.data.PlaceType
import kotlinx.android.synthetic.main.fragment_places.view.*
import kotlinx.android.synthetic.main.item_district.view.*
import kotlinx.android.synthetic.main.item_place_type.view.*

/**
 * Created by keith on 13/03/2018.
 */
class DistrictRecyclerAdapter(data: ArrayList<District>, itemClicked: OnItemClicked<District>) : RecyclerView.Adapter<DistrictRecyclerAdapter.PlacesTypeViewHolder>() {
    var data = data
    var itemClicked = itemClicked
    lateinit var context : Context
    var selectedPosition = -1
    var selectedView : CardView? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesTypeViewHolder {
        context = parent.context
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_district, parent, false)
        return PlacesTypeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: PlacesTypeViewHolder, position: Int) {
        var district = data[position]
        holder.itemView.tv_district_name.setText(district.districtName)
        holder.itemView.cv_district.setOnClickListener { view ->
            if (selectedView != null){
                selectedView!!.setCardBackgroundColor(context.resources.getColor(android.R.color.white))
                selectedView!!.tv_district_name.setTextColor(context.resources.getColor(R.color.black))
            }
            selectedView = view as CardView
            selectedView!!.setCardBackgroundColor(context.resources.getColor(R.color.colorAccent))
            selectedView!!.tv_district_name.setTextColor(context.resources.getColor(android.R.color.white))
            itemClicked.onItemClicked(district, position)
        }
    }

    fun removeSelected(){
        if (selectedView!=null){
            selectedView!!.setCardBackgroundColor(context.resources.getColor(android.R.color.white))
            selectedView!!.tv_district_name.setTextColor(context.resources.getColor(R.color.black))
        }
        selectedView = null
    }

    class PlacesTypeViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }
}