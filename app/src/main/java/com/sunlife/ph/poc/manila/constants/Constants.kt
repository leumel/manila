package com.sunlife.ph.poc.manila.constants

import com.google.android.gms.maps.model.LatLng

/**
 * Created by keith on 08/03/2018.
 */
class Constants (){
    companion object {
        val MAP_KEY = "pk.eyJ1IjoibGVtMjMiLCJhIjoiY2plZXhvZG56MWU0ZjJ4bzc4NTMyZGd2cSJ9.-YjeczKfjs7Gj-OOMOmJlQ"
        val LUNETA_PARK = LatLng(14.583118, 120.979417)
    }
}