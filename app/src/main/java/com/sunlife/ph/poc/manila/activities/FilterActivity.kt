package com.sunlife.ph.poc.manila.activities

import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.sunlife.ph.poc.manila.OnItemClicked
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.adapters.recyclerview.DistrictRecyclerAdapter
import com.sunlife.ph.poc.manila.adapters.recyclerview.PlacesTypeRecyclerAdapter
import com.sunlife.ph.poc.manila.data.District
import com.sunlife.ph.poc.manila.data.PlaceType
import kotlinx.android.synthetic.main.activity_filter.*

/**
 * Created by keith on 13/03/2018.
 */
class FilterActivity : BaseActivity() {
    lateinit var placesTypeRAdapter : PlacesTypeRecyclerAdapter
    lateinit var districtAdapter: DistrictRecyclerAdapter

    override fun getLayout(): Int {
        return R.layout.activity_filter
    }

    override fun initView() {
        loadPlaceType()
        loadDistricts()
        setOnClicks()
    }

    private fun setOnClicks() {
        tv_all_place_type.setOnClickListener {
            placesTypeRAdapter.removeSelected()
            tv_all_place_type.setText("All Selected")
        }

        tv_all_district.setOnClickListener{
            districtAdapter.removeSelected()
            tv_all_district.setText("All Selected")
        }
    }

    private fun loadPlaceType() {
        placesTypeRAdapter = PlacesTypeRecyclerAdapter(
                arrayListOf(PlaceType("Parks" ,R.drawable.bg_park),
                            PlaceType("Restaurants" ,R.drawable.bg_restaurant),
                            PlaceType("Bars" ,R.drawable.bg_bars),
                            PlaceType("Museums" ,R.drawable.bg_museum),
                            PlaceType("Churches" ,R.drawable.bg_church),
                            PlaceType("Government" ,R.drawable.bg_government)),
                            object : OnItemClicked<PlaceType>{
                                override fun onItemClicked(data: PlaceType, position: Int) {
                                    tv_all_place_type.setText("Select All")
                                }
                            })
        var flexLayoutManager = FlexboxLayoutManager(this)
        flexLayoutManager.flexDirection = FlexDirection.ROW
        flexLayoutManager.flexWrap = FlexWrap.WRAP
        flexLayoutManager.justifyContent = JustifyContent.CENTER

        rv_place_type.layoutManager = flexLayoutManager
        rv_place_type.adapter = placesTypeRAdapter
    }


    private fun loadDistricts() {
        districtAdapter = DistrictRecyclerAdapter(
                arrayListOf(District("First District", 1),
                        District("Second District", 1),
                        District("Third District", 1),
                        District("Fourth District", 1),
                        District("Fifth District", 1),
                        District("Sixth District", 1)),
                        object : OnItemClicked<District>{
                            override fun onItemClicked(data: District, position: Int) {
                                tv_all_district.setText("Select All")
                            }
                        })
        var flexLayoutManager = FlexboxLayoutManager(this)
        flexLayoutManager.flexDirection = FlexDirection.ROW
        flexLayoutManager.flexWrap = FlexWrap.WRAP
        flexLayoutManager.justifyContent = JustifyContent.CENTER

        rv_district.layoutManager = flexLayoutManager
        rv_district.adapter = districtAdapter
    }
}