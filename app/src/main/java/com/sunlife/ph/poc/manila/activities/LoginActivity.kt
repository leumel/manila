package com.sunlife.ph.poc.manila.activities

import android.content.Intent
import com.sunlife.ph.poc.manila.R
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by keith on 07/03/2018.
 */
class LoginActivity : BaseActivity() {
    override fun getLayout(): Int {
        return R.layout.activity_login
    }

    override fun initView() {
        tv_skip.setOnClickListener { v ->
            startActivity(Intent(this,MainActivity::class.java))
        }
    }


}