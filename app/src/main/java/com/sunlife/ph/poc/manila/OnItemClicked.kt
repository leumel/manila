package com.sunlife.ph.poc.manila

import com.sunlife.ph.poc.manila.data.PlaceType

/**
 * Created by keith on 13/03/2018.
 */
interface OnItemClicked<K>{
    fun onItemClicked(data:K,position:Int)
}