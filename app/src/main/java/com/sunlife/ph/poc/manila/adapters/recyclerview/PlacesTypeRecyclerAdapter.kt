package com.sunlife.ph.poc.manila.adapters.recyclerview

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager
import com.sunlife.ph.poc.manila.OnItemClicked
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.data.Place
import com.sunlife.ph.poc.manila.data.PlaceType
import kotlinx.android.synthetic.main.fragment_places.view.*
import kotlinx.android.synthetic.main.item_place_type.view.*

/**
 * Created by keith on 13/03/2018.
 */
class PlacesTypeRecyclerAdapter(data: ArrayList<PlaceType>, itemClicked: OnItemClicked<PlaceType>) : RecyclerView.Adapter<PlacesTypeRecyclerAdapter.PlacesTypeViewHolder>() {
    var data = data
    var itemClicked = itemClicked
    lateinit var context : Context
    var selectedPosition = -1
    var selectedView : View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesTypeViewHolder {
        context = parent.context
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_place_type, parent, false)
        var flexLP = view.layoutParams as FlexboxLayoutManager.LayoutParams
        flexLP.flexBasisPercent = 0.45f
        view.layoutParams = flexLP

        return PlacesTypeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: PlacesTypeViewHolder, position: Int) {
        var placeType = data[position]
        var rlSelected = holder.itemView.rl_selected
        holder.itemView.tv_place_type_name.setText(placeType.placeTypeName)
        Glide.with(context).load(placeType.placeTypeImg).into(holder.itemView.iv_place_bg)

        if (selectedPosition == position){
            holder.itemView.rl_selected.visibility = View.VISIBLE
        }else {
            holder.itemView.rl_selected.visibility = View.GONE
        }

        holder.itemView.cv_place_type.setOnClickListener {v->
            if (selectedView!=null){
                selectedView!!.visibility = View.GONE
            }
            rlSelected.visibility = View.VISIBLE
            selectedView = rlSelected
            itemClicked.onItemClicked(placeType, position)
        }
    }

    fun removeSelected(){
        if (selectedView!=null){
            selectedView!!.visibility = View.GONE
        }
        selectedView = null
    }

    class PlacesTypeViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }
}