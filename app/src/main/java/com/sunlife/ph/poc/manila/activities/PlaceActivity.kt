package com.sunlife.ph.poc.manila.activities

import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.MarkerOptions
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.constants.Constants
import kotlinx.android.synthetic.main.fragment_places_details.*

/**
 * Created by keith on 14/03/2018.
 */
class PlaceActivity : BaseActivity(), OnMapReadyCallback {
    override fun getLayout(): Int {
        return R.layout.fragment_places_details
    }


    override fun initView() {
        (map_place_detail as SupportMapFragment).getMapAsync(this)
    }

    override fun onMapReady(gMap: GoogleMap) {
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.LUNETA_PARK, 13.0f))
        var marker = gMap.addMarker(MarkerOptions().title("Somewhere Park").position(Constants.LUNETA_PARK))
        marker.showInfoWindow()
    }
}