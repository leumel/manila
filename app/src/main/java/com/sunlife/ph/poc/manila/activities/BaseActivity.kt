package com.sunlife.ph.poc.manila.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by keith on 07/03/2018.
 */
abstract class BaseActivity:AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        initView()
        onViewCreated()
    }

    open protected fun initView() {
    }

    open protected fun onViewCreated() {
    }

    abstract fun getLayout():Int
}