package com.sunlife.ph.poc.manila.data

/**
 * Created by keith on 13/03/2018.
 */
class Place{
    var placeName = ""
    var placeAddress = ""
    var placeLat : Double = 0.0
    var placeLng : Double = 0.0
    var placeType : Int = -1
    var placeOpeningHour : String = ""
    var placeClosingHour : String = ""
}