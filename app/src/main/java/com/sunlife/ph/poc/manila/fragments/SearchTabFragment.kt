package com.sunlife.ph.poc.manila.fragments

import com.sunlife.ph.poc.manila.R

/**
 * Created by keith on 07/03/2018.
 */
class SearchTabFragment : MainTabFragment() {
    override fun getTabIcon(): Int {
        return R.drawable.ic_search_tab
    }

    override fun getTabIconActive(): Int {
        return R.drawable.ic_search_tab_active
    }

    override fun getTabName(): String {
        return "Search"
    }
}