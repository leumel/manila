package com.sunlife.ph.poc.manila.views

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.R.color.black
import com.sunlife.ph.poc.manila.R.color.red
import kotlinx.android.synthetic.main.item_main_tab.view.*

/**
 * Created by keith on 07/03/2018.
 */
class CustomTab :FrameLayout{
    private var tabIcon : Int
    private var tabIconActive : Int
    private var clickListener : View.OnClickListener

    private var tabName: String

    constructor(context: Context, tabName: String, tabIcon: Int, tabIconActive: Int, clickListener: View.OnClickListener):super(context){
        this.tabName = tabName
        this.tabIcon = tabIcon
        this.tabIconActive = tabIconActive
        this.clickListener = clickListener
        initTab()
    }

    private fun initTab(){
        LayoutInflater.from(context).inflate(R.layout.item_main_tab, this)
        tv_title.text = tabName
        toggle(false)
    }

    fun toggle(activate:Boolean){
        var color = black
        var drawable = tabIcon
        if (activate){
            color = red
            drawable = tabIconActive
        }
        tv_title.setTextColor(context.resources.getColor(color))
        iv_tab.setImageDrawable(context.resources.getDrawable(drawable))
    }
}