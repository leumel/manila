package com.sunlife.ph.poc.manila.fragments

/**
 * Created by keith on 07/03/2018.
 */
abstract class MainTabFragment : BaseFragment(){
    abstract fun getTabName() : String
    abstract fun getTabIcon() : Int
    abstract fun getTabIconActive() : Int
}