package com.sunlife.ph.poc.manila.fragments

import android.animation.ValueAnimator
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.DecelerateInterpolator
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.data.Feature
import com.google.maps.android.data.Layer
import com.google.maps.android.data.geojson.GeoJsonFeature
import com.google.maps.android.data.geojson.GeoJsonLayer
import com.google.maps.android.data.geojson.GeoJsonLineStringStyle
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.activities.FilterActivity
import com.sunlife.ph.poc.manila.constants.Constants
import kotlinx.android.synthetic.main.fragment_places.*

/**
 * Created by keith on 07/03/2018.
 */
class PlacesTabFragment : MainTabFragment(), OnMapReadyCallback {
    val ZOOM = 13.0f
    val ZOOM_MAX = 16.0f

    val ANIM_DURATION: Long = 300
    val ACTIVATED_ALPHA = 200
    val DEACTIVATED_ALPHA = 100


    var lastSelectedDistrict = "all"

    lateinit var googleMap: GoogleMap
    var originalFilterYStart = -1.0f
    var originalTopPlaceListStart = -1

    var originalFilterYEnd = -1.0f
    var originalTopPlaceListEnd = -1

    var hideList = false
    var isDisctrictsHidden = false

    lateinit var geoJsonLayer: GeoJsonLayer
    var rootView: View? = null

    var geoAdded = false

    var districtColors = arrayOf(arrayListOf(43, 45, 66), arrayListOf(250, 166, 19),
            arrayListOf(114, 87, 82), arrayListOf(190, 178, 200),
            arrayListOf(216, 0, 50), arrayListOf(77, 161, 103))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = LayoutInflater.from(context).inflate(R.layout.fragment_places, container, false)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (map as SupportMapFragment).getMapAsync(this)
        view.viewTreeObserver.addOnGlobalLayoutListener(
                object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        view.viewTreeObserver.removeGlobalOnLayoutListener(this)
                        originalFilterYStart = rl_filter.y
                        originalTopPlaceListStart = cv_place_list.top

                        originalFilterYEnd = cv_place_list.bottom * 1.0f - rl_filter.height - (rl_filter.layoutParams as ConstraintLayout.LayoutParams).bottomMargin
                        originalTopPlaceListEnd = cv_place_list.bottom
                    }
                }
        )
        setOnClicks()
    }

    private fun setOnClicks() {
        cv_filter.setOnClickListener { v ->
//            hideList = !hideList
//            toggleList(hideList)
            startActivity(Intent(activity, FilterActivity::class.java))
        }
    }

    private fun toggleList(hide: Boolean) {
        var startListSet = originalTopPlaceListStart
        var startFilter = originalFilterYStart

        var endLists = originalTopPlaceListEnd
        var endFilter = originalFilterYEnd

        if (!hide) {
            startListSet = originalTopPlaceListEnd
            startFilter = originalFilterYEnd

            endLists = originalTopPlaceListStart
            endFilter = originalFilterYStart
        }

        val animatorPlace = ValueAnimator.ofFloat(startListSet * 1.0f, endLists * 1.0f)
        val animatorFilter = ValueAnimator.ofFloat(startFilter, endFilter)

        animatorPlace.addUpdateListener(
                object : ValueAnimator.AnimatorUpdateListener {
                    override fun onAnimationUpdate(p0: ValueAnimator?) {
                        cv_place_list.y = p0?.animatedValue as Float
                    }
                }
        )

        animatorFilter.addUpdateListener(
                object : ValueAnimator.AnimatorUpdateListener {
                    override fun onAnimationUpdate(p0: ValueAnimator?) {
                        rl_filter.y = p0?.animatedValue as Float
                    }
                }
        )

        animatorPlace.setDuration(ANIM_DURATION)
        animatorFilter.setDuration(ANIM_DURATION)

        animatorPlace.interpolator = DecelerateInterpolator()
        animatorFilter.interpolator = DecelerateInterpolator()

        animatorPlace.start()
        animatorFilter.start()
        resetMap()
    }

    override fun getTabIcon(): Int {
        return R.drawable.ic_pin_tab
    }

    override fun getTabIconActive(): Int {
        return R.drawable.ic_pin_tab_active
    }

    override fun getTabName(): String {
        return "Places"
    }

    override fun onMapReady(gMap: GoogleMap) {
        Log.d("lem", "called")
        googleMap = gMap
        googleMap.clear()
        googleMap.setOnCameraMoveListener { Log.d("lem", "Moved") }

        googleMap.setOnCameraIdleListener {
            if (ZOOM_MAX <= googleMap.cameraPosition.zoom) {
                geoJsonLayer.removeLayerFromMap()
                isDisctrictsHidden = true
            } else if (isDisctrictsHidden) {
                isDisctrictsHidden = false
                geoJsonLayer.addLayerToMap()
            }
        }

        initializeGeoLayer()
        geoJsonLayer.setOnFeatureClickListener(
                object : Layer.OnFeatureClickListener {
                    override fun onFeatureClick(feature: Feature) {
                        for (geoFeature in geoJsonLayer.features) {
                            if (geoFeature.id.equals(lastSelectedDistrict)) {
                                updateGeoFeatureStyle(geoFeature as GeoJsonFeature, DEACTIVATED_ALPHA, getDistrictColor(geoFeature.id))
                            }
                        }
                        lastSelectedDistrict = feature.id
                        updateGeoFeatureStyle(feature as GeoJsonFeature, ACTIVATED_ALPHA, getDistrictColor(feature.id))
                    }
                }
        )
        geoJsonLayer.addLayerToMap()
        resetMap()
    }

    private fun resetMap() {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.LUNETA_PARK, ZOOM))
        for (geoFeature in geoJsonLayer.features){
            var geoFeature = geoFeature as GeoJsonFeature
            if (geoFeature.id.equals("fifth")){
                lastSelectedDistrict = "fifth"
                updateGeoFeatureStyle(geoFeature, ACTIVATED_ALPHA,getDistrictColor("fifth"))
            }
        }
        var lunetaPark = googleMap.addMarker(MarkerOptions().position(Constants.LUNETA_PARK).title("Luneta Park"))
        lunetaPark.showInfoWindow()
    }

    private fun getDistrictColor(id: String): ArrayList<Int> {
        var index = 0
        when (id) {
            "first" -> {
                index = 0
            }
            "second" -> {
                index = 1
            }
            "third" -> {
                index = 2
            }
            "fourth" -> {
                index = 3
            }
            "fifth" -> {
                index = 4
            }
            "sixth" -> {
                index = 5
            }
        }
        Log.d("lem", id+" "+index)
        return districtColors[index]
    }

    private fun initializeGeoLayer() {
        geoJsonLayer = GeoJsonLayer(googleMap, R.raw.manila_al7, activity)

        for ((index, geoFeature) in geoJsonLayer.features.withIndex()) {
            var districtColor = getDistrictColor((geoFeature as GeoJsonFeature).id)
            updateGeoFeatureStyle(geoFeature as GeoJsonFeature, DEACTIVATED_ALPHA, districtColor)
        }
    }

    private fun updateGeoFeatureStyle(geoFeature: GeoJsonFeature, alpha: Int, argb: ArrayList<Int>) {
        val geojsonPolygonStyle = GeoJsonPolygonStyle()
        geojsonPolygonStyle.strokeColor = Color.WHITE
        geojsonPolygonStyle.strokeWidth = 5.0f
        geojsonPolygonStyle.fillColor = Color.argb(alpha, argb[0], argb[1], argb[2])
        geoFeature.polygonStyle = geojsonPolygonStyle
    }
}