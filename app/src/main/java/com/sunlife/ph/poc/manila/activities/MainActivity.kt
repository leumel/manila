package com.sunlife.ph.poc.manila.activities

import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.View
import com.sunlife.ph.poc.manila.R
import com.sunlife.ph.poc.manila.adapters.fragmentpager.CustomFragmentPagerAdapter
import com.sunlife.ph.poc.manila.fragments.*
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by keith on 07/03/2018.
 */
class MainActivity : BaseActivity(), View.OnClickListener {
    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun initView() {
        var customFragmentPagerAdapter = CustomFragmentPagerAdapter(getMainFragments(), this, supportFragmentManager, this)
        vp.adapter = customFragmentPagerAdapter

        tabs.setupWithViewPager(vp)
        customFragmentPagerAdapter.setupTabs(tabs)
    }


    fun getMainFragments(): List<MainTabFragment> {
        var fragments = ArrayList<MainTabFragment>()
        fragments.add(PlacesTabFragment())
        fragments.add(SearchTabFragment())
        fragments.add(ProfileTabFragment())
        return fragments
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}