package com.sunlife.ph.poc.manila.views

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by keith on 12/03/2018.
 */
class CustomViewPager : ViewPager{
    constructor(context: Context):super(context){

    }

    constructor(context: Context, attrs : AttributeSet):super(context, attrs){

    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }

    override fun setCurrentItem(item: Int) {
        setCurrentItem(item, false)
    }

    override fun setCurrentItem(item: Int, smoothScroll: Boolean) {
        super.setCurrentItem(item,false)
    }

}